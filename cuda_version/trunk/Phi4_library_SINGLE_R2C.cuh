/*
 * Project started on: Jul 11 2011 by ezeferrero
 * Copyright (C) 2012 2013 Ezequiel E. Ferrero, Alejandro B. Kolton and Sebastián Bustingorry.
 */

#ifndef PHI4_LIBRARY_CUH
#define PHI4_LIBRARY_CUH

//#include "../common/common.c"
#include <stdint.h> /* uint8_t */

using namespace std;

texture<float,1,cudaReadModeElementType> qmoduleTex;
texture<float,1,cudaReadModeElementType> disorderTex;

//---------- DEVICE FUNCTIONS -----------//

__global__ void CUDAkernel_InitRandom(cufftReal *a, int nx, int ny){
	const unsigned int jAux = blockIdx.x*TILE_X + threadIdx.x;
	const unsigned int iAux = blockIdx.y*TILE_Y + threadIdx.y;
	const unsigned int tid = iAux*FRAME + jAux;
	// move thread RNG state to registers. 
	unsigned long long rng_state = d_x[tid];
	const unsigned int rng_const = d_a[tid];

	unsigned int i;	
	for (unsigned int iFrame=0; iFrame<(nx/FRAME); iFrame++) {
		i = iAux + FRAME*iFrame;
		unsigned int j;
		for (unsigned int jFrame=0; jFrame<(ny/FRAME); jFrame++) {
			j = jAux + FRAME*jFrame;
			// compute index with i and j, the location of the element in the original LX*LY array 
			int index = i*ny + j;
			a[index] = 2.f*(rand_MWC_co(&rng_state, &rng_const)-0.5);
		}
	}
	d_x[tid] = rng_state; // store RNG state into global again
}

__global__ void CUDAkernel_SetQxQy(float *qqmodule, int nx, int ny){
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		// Using cos(qx) cos(qy) boundary conditions are automatically determined by the function \
		periodicity otherwhise modulo "%" operations are required.
		float qx=2.f*M_PI/(float)LX*idx;
		float qy=2.f*M_PI/(float)LY*idy;
		int index = idx*ny + idy;
		qqmodule[index]=-2.f*(cosf(qx)+cosf(qy)-2.f);

		//int nxs2=(nx/2); int nys2=(ny/2);
		//float qx=2*M_PI/(float)LX*((idx+nxs2)-nx);
		//float qy=2*M_PI/(float)LY*((idy+nys2)-ny);
		//int index = ((idx+nxs2)%nx)*ny + ((idy+nys2)%ny); 
		//qqmodule[index]=qx*qx+qy*qy;
	}
}

__global__ void CUDAkernel_SetDisorder(float *d_disorder, int nx, int ny){
	const unsigned int jAux = blockIdx.x*TILE_X + threadIdx.x;
	const unsigned int iAux = blockIdx.y*TILE_Y + threadIdx.y;
	const unsigned int tid = iAux*FRAME + jAux;
	// move thread RNG state to registers. 
	unsigned long long rng_state = d_x[tid];
	const unsigned int rng_const = d_a[tid];

	unsigned int i;
	for (unsigned int iFrame=0; iFrame<(nx/FRAME); iFrame++) {
		i = iAux + FRAME*iFrame;
		unsigned int j;
		for (unsigned int jFrame=0; jFrame<(ny/FRAME); jFrame++) {
			j = jAux + FRAME*jFrame;
			// compute index with i and j, the location of the element in the original LX*LY array 
			int index = i*ny + j;
//			d_disorder[index] = C_ALPHA + C_EPSILON*2.f*(rand_MWC_co(&rng_state, &rng_const)-0.5);
			d_disorder[index] = C_EPSILON*3.46410162*(rand_MWC_co(&rng_state, &rng_const)-0.5);
		}
	}	
	d_x[tid] = rng_state; // store RNG state into global again
}

__global__ void CUDAkernel_Real2RealScaled(cufftReal *a, int nx, int ny, float scale){
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		int index = idx*ny + idy;
		a[index]= scale*a[index];
	}	
}

__global__ void complex2complex_scaled(cufftComplex *a, int nx, int ny, float scale){
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		int index = idx*ny + idy;
		a[index].x= scale*a[index].x;
		a[index].y= scale*a[index].y;
	}	
}


__global__ void CUDAkernel_CalculateRealForce(const float *e, const cufftReal *a, cufftReal *b, int nx, int ny, \
				float uniform_field){
	const unsigned int jAux = blockIdx.x*TILE_X + threadIdx.x;
	const unsigned int iAux = blockIdx.y*TILE_Y + threadIdx.y;
	const unsigned int tid = iAux*FRAME + jAux;
	// move thread RNG state to registers. 
	unsigned long long rng_state = d_x[tid];
	const unsigned int rng_const = d_a[tid];

	int i;
	for (unsigned int iFrame=0; iFrame<(nx/FRAME); iFrame++) {
		i = iAux + FRAME*iFrame;
		int j;
		for (unsigned int jFrame=0; jFrame<(ny/FRAME); jFrame++) {
			j = jAux + FRAME*jFrame;
			// compute index with i and j, the location of the element in the original LX*LY array 
			int index = i*ny + j;
// 			const float epsilon = e[index];
			const float epsilon = tex1Dfetch(disorderTex, index);
			//b[index] = epsilon*(a[index]-a[index]*a[index]*a[index]) + \
			XTEMP*(rand_MWC_co(&rng_state, &rng_const)-0.5) + uniform_field;
			//--> changed on 11/1/2013 to -->
//			b[index] = ((C_ALPHA+epsilon)*a[index]-a[index]*a[index]*a[index]) + \
			XTEMP*(rand_MWC_co(&rng_state, &rng_const)-0.5) + uniform_field;
			#ifndef RADIALFORCE
			b[index] = C_ALPHA*((1+epsilon)*a[index]-a[index]*a[index]*a[index]) + \
			XTEMP*(rand_MWC_co(&rng_state, &rng_const)-0.5) + uniform_field;
			#else
			const float dist_to_center = (sqrtf((i-nx/2)*(i-nx/2) + \
					(j-ny/2)*(j-ny/2)))*((i!=nx/2)||(j!=ny/2))+((i==nx/2)&&(j==ny/2));
			b[index] = C_ALPHA*((1+epsilon)*a[index]-a[index]*a[index]*a[index]) + \
			XTEMP*(rand_MWC_co(&rng_state, &rng_const)-0.5) + uniform_field/dist_to_center;
			#endif
		}
	}
	d_x[tid] = rng_state; // store RNG state into global again
}

#ifdef STRUCTUREFACTOR
__global__ void CUDAkernel_ComputeStructureFactor(const cufftComplex *d_phi, cufftReal *d_Sq2d, int nx, int ny){
	// compute idx and idy, the location of the element in the original NxN array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx  && idy < ny){
		//int index = idx*ny + idy;
		unsigned int k=idx*(LY/2+1)+idy;
		unsigned int kk=idx*LY+idy;
		unsigned int kkk=(idx+1)*LY-idy;
		d_Sq2d[kk] = (d_phi[k].x*d_phi[k].x+d_phi[k].y*d_phi[k].y)/(float)(LX*LY);
		d_Sq2d[kkk] = d_Sq2d[kk];
	}
}

__global__ void CUDAkernel_ComputeStructureFactorImage(const cufftReal *d_Sq2d, cufftReal *d_Sq2dimage, int nx, int ny){
	// compute idx and idy, the location of the element in the original NxN array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx  && idy < ny){
		unsigned int k=idx*LY+idy;
		int ii=(idx>LX/2)?(idx-LX/2):(idx+LX/2);
		int jj=(idy>LY/2)?(idy-LY/2):(idy+LY/2);
		unsigned int kk=ii*LY+jj;
		d_Sq2dimage[kk]=d_Sq2d[k];
	}
}
#endif

__global__ void euler_step(cufftComplex *a, const cufftComplex *b, const float *qqmodule, int nx, int ny){
	// compute idx and idy, the location of the element in the original NxN array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		int index = idx*ny + idy;
		//const float fac=-(qqmodule[index]);
		const float fac=-(tex1Dfetch(qmoduleTex, index));
		const float dip = -(9.05f-2.f*M_PI*sqrtf(-1.f*fac));
		a[index].x = (1.f+C_BETA*fac*DT+C_GAMMA*dip*DT)*a[index].x + DT*b[index].x;
		a[index].y = (1.f+C_BETA*fac*DT+C_GAMMA*dip*DT)*a[index].y + DT*b[index].y;
	}	
}

__global__ void euler_semiimplicit_step(cufftComplex *a, const cufftComplex *b, const float *qqmodule, int nx, int ny){
	// compute idx and idy, the location of the element in the original NxN array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx  && idy < ny){
		int index = idx*ny + idy;
		//const float fac=-(qqmodule[index]);
		const float fac=-(tex1Dfetch(qmoduleTex, index));
		const float dip = -(9.05f-2.f*M_PI*sqrtf(-1.f*fac));
		a[index].x = (a[index].x*(1.f+C_GAMMA*dip*DT) + DT*b[index].x)/(1.f-fac*C_BETA*DT);
		a[index].y = (a[index].y*(1.f+C_GAMMA*dip*DT) + DT*b[index].y)/(1.f-fac*C_BETA*DT);
	}
}


/* RGB formulae.
 * http://gnuplot-tricks.blogspot.com/2009/06/comment-on-phonged-surfaces-in-gnuplot.html
 * TODO: cannot find what to do with over/under flows, currently saturate in [0,255]
 *
 * there are 37 available rgb color mapping formulae:
 * 0: 0               1: 0.5             2: 1
 * 3: x               4: x^2             5: x^3
 * 6: x^4             7: sqrt(x)         8: sqrt(sqrt(x))
 * 9: sin(90x)        10: cos(90x)       11: |x-0.5|
 * 12: (2x-1)^2       13: sin(180x)      14: |cos(180x)|
 * 15: sin(360x)      16: cos(360x)      17: |sin(360x)|
 * 18: |cos(360x)|    19: |sin(720x)|    20: |cos(720x)|
 * 21: 3x             22: 3x-1           23: 3x-2
 * 24: |3x-1|         25: |3x-2|         26: (3x-1)/2
 * 27: (3x-2)/2       28: |(3x-1)/2|     29: |(3x-2)/2|
 * 30: x/0.32-0.78125 31: 2*x-0.84       32: 4x;1;-2x+1.84;x/0.08-11.5
 * 33: |2*x - 0.5|    34: 2*x            35: 2*x - 0.5
 * 36: 2*x - 1
 * 
 * Some nice schemes in RGB color space
 * 7,5,15   ... traditional pm3d (black-blue-red-yellow)
 * 3,11,6   ... green-red-violet
 * 23,28,3  ... ocean (green-blue-white); try also all other permutations
 * 21,22,23 ... hot (black-red-yellow-white)
 * 30,31,32 ... color printable on gray (black-blue-violet-yellow-white)
 * 33,13,10 ... rainbow (blue-green-yellow-red)
 * 34,35,36 ... AFM hot (black-red-yellow-white)
 */

#define RGBf05 (x*x*x)
#define RGBf07 (sqrt(x))
#define RGBf15 (sin(2*M_PI*x))
#define RGBf21 (3*x)
#define RGBf22 (3*x-1)
#define RGBf23 (3*x-2)
#define RGBf30 (x/0.32-0.78125)
#define RGBf31 (2*x-0.84)
#define RGBf32 (x/0.08-11.5)

#define MAX_COMPONENT_VALUE 255
//void writePPMbinaryImage(const char * filename, const cufftComplex * vector)
void writePPMbinaryImage(const char * filename, const cufftReal * vector, int whatis)
{
	FILE *f = NULL;
	unsigned int i = 0, j = 0;
	uint8_t RGB[3] = {0,0,0}; /* use 3 first bytes for BGR */ 
	f = fopen(filename, "w");
	assert(f);
	fprintf(f, "P6\n"); /* Portable colormap, binary */
	fprintf(f, "%d %d\n", LY, LX); /* Image size */
	fprintf(f, "%d\n", MAX_COMPONENT_VALUE); /* Max component value */
	for (i=0; i<LX; i++){
		for (j=0; j<LY; j++){
			/* ColorMaps
			 * http://mainline.brynmawr.edu/Courses/cs120/spring2008/Labs/ColorMaps/colorMap.py
			 */
			//float x = vector[i*LY+j].x; //HOT; /* [0,1] */
			float x;
			if (whatis==0) x = (vector[i*LY+j]+1)/2.; //HOT; /* [0,1] */
			if (whatis==1) x = log(vector[i*LY+j]); //HOT; /* [0,1] */
//			RGB[0] = (uint8_t) MIN(255, MAX(0, 256*RGBf21));
//			RGB[1] = (uint8_t) MIN(255, MAX(0, 256*RGBf22));
//			RGB[2] = (uint8_t) MIN(255, MAX(0, 256*RGBf23));
			RGB[0] = (uint8_t) MIN(255, MAX(0, 256*RGBf07));  //21
			RGB[1] = (uint8_t) MIN(255, MAX(0, 256*RGBf05));  //22
			RGB[2] = (uint8_t) MIN(255, MAX(0, 256*RGBf15));  //23
			//assert(0<=RGB[0] && RGB[0]<256);
			//assert(0<=RGB[1] && RGB[1]<256);
			//assert(0<=RGB[2] && RGB[2]<256);
			fwrite((void *)RGB, 3, 1, f);
		}
	}
	fclose(f); f = NULL;
	return;
}

//////////////////////////////////////////////////////////////////////
class phi4_model
{
	private:
	// cuFFT plan
	cufftHandle Sr2c, Sc2r; 

	public:

	// real arrays to hold the scalar field phi and the force
	cufftReal *h_phi_r, *h_force_r;
	cufftReal *d_phi_r, *d_force_r;
	#ifdef STRUCTUREFACTOR
	cufftReal *d_Sq2d, *d_Sq2dimage;
	cufftReal *h_Sq2dimage;
	#endif

	// complex arrays to hold the transformed scalar field F[phi] and force F[force]
	cufftComplex *d_phi, *d_force; 
	
	// float array for the Fourier modes weights (the q's)
	float *d_qmodule; 
	
	// float array for the quenched disorder
	float *d_disorder;
	
	//TODO:Arrays for the physical quantities
	
	// intializing the class
	phi4_model(){

		h_phi_r = (cufftReal *)malloc(sizeof(cufftReal)*LX*LY);
		h_force_r = (cufftReal *)malloc(sizeof(cufftReal)*LX*LY);
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_phi_r, sizeof(cufftReal)*LX*LY));
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_force_r, sizeof(cufftReal)*LX*LY));
		#ifdef STRUCTUREFACTOR
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_Sq2d, sizeof(cufftReal)*LX*LY));
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_Sq2dimage, sizeof(cufftReal)*LX*LY));
		h_Sq2dimage = (cufftReal *)malloc(sizeof(cufftReal)*LX*LY);
		#endif

		CUDA_SAFE_CALL(cudaMalloc((void**)&d_phi, sizeof(cufftComplex)*LX*(LY/2+1)));
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_force, sizeof(cufftComplex)*LX*(LY/2+1)));
		
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_qmodule, sizeof(float)*LX*(LY/2+1))); 

		CUDA_SAFE_CALL(cudaMalloc((void**)&d_disorder, sizeof(float)*LX*LY)); 

		/* Fill everything with 0s */

		for (unsigned int k=0; k<LX*LY; k++){
			h_phi_r[k] = (cufftReal) 0;
			h_force_r[k] = (cufftReal) 0;
		#ifdef STRUCTUREFACTOR
			h_Sq2dimage[k] = (cufftReal) 0;
		#endif
		}

		CUDA_SAFE_CALL(cudaMemset(d_phi_r, 0, LX*LY*sizeof(cufftReal)));
		CUDA_SAFE_CALL(cudaMemset(d_force_r, 0, LX*LY*sizeof(cufftReal))); 
		#ifdef STRUCTUREFACTOR
		CUDA_SAFE_CALL(cudaMemset(d_Sq2d, 0, LX*LY*sizeof(cufftReal))); 
		CUDA_SAFE_CALL(cudaMemset(d_Sq2dimage, 0, LX*LY*sizeof(cufftReal))); 
		#endif

		CUDA_SAFE_CALL(cudaMemset(d_phi, 0, LX*(LY/2+1)*sizeof(cufftComplex)));
		CUDA_SAFE_CALL(cudaMemset(d_force, 0, LX*(LY/2+1)*sizeof(cufftComplex)));

		CUDA_SAFE_CALL(cudaMemset(d_qmodule, 0, LX*(LY/2+1)*sizeof(float)));
		CUDA_SAFE_CALL(cudaMemset(d_disorder, 0, LX*LY*sizeof(float))); 

		// cuFFT plans R2C and C2R
		CUFFT_SAFE_CALL(cufftPlan2d(&Sr2c,LX,LY,CUFFT_R2C));
		CUFFT_SAFE_CALL(cufftPlan2d(&Sc2r,LX,LY,CUFFT_C2R));
	//	cufftSetCompatibilityMode(Sr2c, CUFFT_COMPATIBILITY_NATIVE);
	//	cufftSetCompatibilityMode(Sc2r, CUFFT_COMPATIBILITY_NATIVE);

	}


////////// Initializing Functions //////////

	void InitParticular(const int nx, const int ny, const int shape, const float x_offset, const float y_offset, \
			const float width, const float angle){
		for(int i=0;i<nx;i++){
			for(int j=0;j<ny;j++){
				int k=i*ny+j;

				// flat interfase
				if (shape==0) h_phi_r[k] = (ny*(0.5+y_offset) < j)?(1.0):(-1.0);
	
				// band
				if (shape==1) h_phi_r[k] = (ny*(0.5-width*0.5+y_offset) < j && \
							j < ny*(0.5+width*0.5+y_offset))?(1.0):(-1.0);
				//h_phi_r[k]= (i+j<ny*0.5 || (nx-i)+(ny-j)<ny*0.5)?(1.0):(-1.0);

				// circle
 				if (shape==2){
				const float a = i-nx*(0.5+x_offset);
				const float b = j-ny*(0.5+y_offset);
				const float c = ny*width*0.5;
				h_phi_r[k] = ( a*a + b*b < c*c )?(1.0):(-1.0);
				}

				// square_with_hole
				// TODO: generalize this case if desired
 				if (shape==3){
				h_phi_r[k]= (nx*0.35 < i && i < nx*0.65 && ny*0.35 < j && \
								j < ny*0.65)?(1.0):(0.0);
				h_phi_r[k]= (nx*0.45 < i && i < nx*0.55 && ny*0.45 < j && \
								j < ny*0.55)?(0.0):(h_phi_r[k]);
				}

				// parallel stripes
				// TODO: Think about preserving periodic boundary conditions
				if (shape==4) h_phi_r[k]= \
				(cosf((sinf(angle)*j+cosf(angle)*i)*2*M_PI/(width*ny))> 0.f)?(1.0):(-1.0);
			}
		}
 		CpyHostToDevice();
	};

	void InitRandom(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(FRAME/TILE_X, FRAME/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		CUDAkernel_InitRandom<<<dimGrid, dimBlock>>>(d_phi_r,LX,LY);
	};

	void InitFlat(const cufftReal value){
		CUDA_SAFE_CALL(cudaMemset(d_phi_r, value, LX*LY*sizeof(cufftReal)));
	};

	void SetQxQy(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		CUDAkernel_SetQxQy<<<dimGrid, dimBlock>>>(d_qmodule,LX,LY/2+1);
		CUDA_SAFE_CALL(cudaBindTexture(NULL, qmoduleTex, d_qmodule, LX*(LY/2+1)*sizeof(float)));
		
	};

	void SetDisorder(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(FRAME/TILE_X, FRAME/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		CUDAkernel_SetDisorder<<<dimGrid, dimBlock>>>(d_disorder,LX,LY);
		CUDA_SAFE_CALL(cudaBindTexture(NULL, disorderTex, d_disorder, LX*LY*sizeof(float)));
	};


////////// Copy Functions //////////

	/* transfer from CPU to GPU memory */
	void CpyHostToDevice(){
		CUDA_SAFE_CALL(cudaMemcpy(d_phi_r,h_phi_r, sizeof(cufftReal)*LX*LY, cudaMemcpyHostToDevice));
		CUDA_SAFE_CALL(cudaMemcpy(d_force_r,h_force_r, sizeof(cufftReal)*LX*LY, cudaMemcpyHostToDevice));
	}

	/* transfer from GPU to CPU memory */
	void CpyDeviceToHost(){
		CUDA_SAFE_CALL(cudaMemcpy(h_phi_r, d_phi_r, sizeof(cufftReal)*LX*LY, cudaMemcpyDeviceToHost));
		#ifdef STRUCTUREFACTOR
		CUDA_SAFE_CALL(cudaMemcpy(h_Sq2dimage, d_Sq2dimage, sizeof(cufftReal)*LX*LY, cudaMemcpyDeviceToHost));
		#endif
	}

////////// Fourier Transform Functions //////////

	void TransformToFourierSpace(){
		CUFFT_SAFE_CALL(cufftExecR2C(Sr2c, d_phi_r, d_phi));
		CUFFT_SAFE_CALL(cufftExecR2C(Sr2c, d_force_r, d_force));
	};

	void AntitransformFromFourierSpace(){
		CUFFT_SAFE_CALL(cufftExecC2R(Sc2r, d_phi, d_phi_r));
	};

	/* apply scaling ( an FFT followed by iFFT will give you back the same array times the length of the transform) */
	void Normalize(void){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		CUDAkernel_Real2RealScaled<<<dimGrid, dimBlock>>>(d_phi_r, LX, LY, 1.f / ((float) LX * (float) LY));
	}

////////// Computing Functions //////////
	#ifdef STRUCTUREFACTOR
	void ComputeStructureFactor(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		CUDAkernel_ComputeStructureFactor<<<dimGrid, dimBlock>>>(d_phi, d_Sq2d, LX, (LY/2+1));
	};

	void ComputeStructureFactorImage(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		CUDAkernel_ComputeStructureFactorImage<<<dimGrid, dimBlock>>>(d_Sq2d, d_Sq2dimage, LX, LY);
	};

	#endif
////////// Evolution Functions //////////

	void CalculateRealForce(float uniform_field){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(FRAME/TILE_X, FRAME/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		CUDAkernel_CalculateRealForce<<<dimGrid, dimBlock>>>(d_disorder,d_phi_r,d_force_r,LX,LY,uniform_field);
	}

	void EulerStep(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		euler_step<<<dimGrid, dimBlock>>>(d_phi, d_force, d_qmodule, LX, (LY/2+1));
	};
	
	void EulerStepSemiimplicit(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		euler_semiimplicit_step<<<dimGrid, dimBlock>>>(d_phi, d_force, d_qmodule, LX, (LY/2+1));
	};

////////// Print Functions //////////

	void PrintData(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << setprecision (4) << h_phi_r[k] << " ";
			}
			fstr << endl;
		}
		fstr << endl;
	};

	void PrintPicture(char *picturename, int x){
		writePPMbinaryImage(picturename, h_phi_r, 0);
		#ifdef JPEG
		char cmd[200];
		sprintf(cmd, "convert frame%d.ppm frame%d.jpg", 100000000+x, 100000000+x);
		system(cmd);
		sprintf(cmd, "rm frame%d.ppm", 100000000+x);
		system(cmd);
		#endif
	};

	#ifdef STRUCTUREFACTOR
	void PrintStructureFactor(char *picturename, int x){
		char cmd[200];
		writePPMbinaryImage(picturename, h_Sq2dimage, 1);
		sprintf(cmd, "convert Sq%d.ppm -resize %dx%d\\! Sq%d.ppm", 100000000+x, LX, LX, 100000000+x);
		int i = system(cmd);
		#ifdef JPEG
		sprintf(cmd, "convert Sq%d.ppm Sq%d.jpg", 100000000+x, 100000000+x);
		system(cmd);
		sprintf(cmd, "rm Sq%d.ppm", 100000000+x);
		system(cmd);
		#endif
	}
	#endif

/////////////// Cleaning Issues ////////////////

	~phi4_model(){
		cufftDestroy(Sr2c);
		cufftDestroy(Sc2r);
		cudaFree(d_phi_r);
		cudaFree(d_force_r);
		free(h_phi_r);
		free(h_force_r);
		cudaFree(d_phi);
		cudaFree(d_force);
		CUDA_SAFE_CALL(cudaUnbindTexture(disorderTex));
		CUDA_SAFE_CALL(cudaUnbindTexture(qmoduleTex));
		cudaFree(d_qmodule);
		cudaFree(d_disorder);
	};
};

#endif /* PHI4_LIBRARY_CUH */
