// main
/*
This program simulates the so called Phi4 Model for a bidimensional scalar field
It includes:
- A local double-well potential
- A rigidity term or local nearest neighbors interactions through a term proportional to the Laplacian of the scalar field
- An external uniform driving force 
- Quenched disorder impurities
- Long-range dipolar interactions
*/

#include <cmath>
#include <assert.h>
#include <sys/time.h>	/* gettimeofday */

#include <cuda.h>
#include <cuda_runtime.h>
#include "cutil.h"	// CUDA_SAFE_CALL, CUT_CHECK_ERROR
#include <cufft.h>

#include <stdio.h>	/* print */
#include <stdlib.h>
#include <iostream>	/* print */
#include <iomanip>	/* print */
#include <fstream>	/* print */

// PARAMETERS

#ifndef LX
#define LX 512			/* System's X size*/
#endif

#ifndef LY
#define LY 512			/* System's Y size*/
#endif

#ifndef T_RUN
#define	T_RUN 1000		/* Running time */
#endif

#ifndef T_DATA
#define T_DATA 100		/* Data acquisition time */
#endif

#ifndef DT
#define DT 0.05f			/* Integration step time */
#endif

#ifndef C_ALPHA
#define C_ALPHA 0.8f		/* Phi4 potential prefactor */
#endif

#ifndef C_BETA
#define C_BETA 2.0f		/* Laplacian prefactor */
#endif

#ifndef C_EPSILON
#define C_EPSILON 0.1f		/* Intensity of the disorder potential */
#endif

#ifndef TEMP
#define TEMP 0.f		/* Heat Bath temperature*/
#endif
//#define	XTEMP sqrt(24.*TEMP*DT)
#define	XTEMP sqrt(24.*TEMP/DT) // Langevin term will be multiplied by DT at the integration step.

#ifndef H_START
#define H_START 0.1f		/* Uniform external field */
#endif

#ifndef VELOCITY
#define VELOCITY 0.01f
#endif

#ifndef CUDA_DEVICE
#define CUDA_DEVICE 0		/* GPU to be used ID */
#endif

#define n_umbral -0.5f
#define p_umbral 0.5f

// Functions
#define MAX(a,b) (((a)<(b))?(b):(a))	// maximum
#define MIN(a,b) (((a)<(b))?(a):(b))	// minimum
#define ABS(a) (((a)< 0)?(-a):(a))	// absolute value

// Hardware parameters for GTX 470/480 (GF100)
#define SHARED_PER_BLOCK 49152
#define WARP_SIZE 32
#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_GRID 65535

// RNG: multiply with carry
#include "../common/CUDAMCMLrng.cu"

// Parameters for the RNG
#define FRAME 256	// the whole thing is framed for the RNG
#define TILE_X 8	// each block of threads is a tile
#define TILE_Y 32	// each block of threads is a tile
#define NUM_THREADS (FRAME*FRAME)
#define MICROSEC (1E-6)

#ifndef SEED
	#ifndef PHILOX
	#define SEED time(NULL)
	#else
	#define SEED 1332277027LLU
	#endif
#endif

#ifdef DOUBLE_PRECISION
typedef double FLOAT;
#else
typedef float FLOAT;
#endif

int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);

#ifndef PHILOX

	// Safe-Primes file used by the MWC RNG
	#define SAFE_PRIMES_FILENAME "../common/safeprimes_base32.txt"
	//#define SAFE_PRIMES_FILENAME "../common/safeprimes_base32_plus.txt"

	// state of the random number generator, last number (x_n), last carry (c_n) packed in 64 bits
	__device__ static unsigned long long d_x[NUM_THREADS];
	// multipliers (constants)
	__device__ static unsigned int d_a[NUM_THREADS];

	// Including our functions
	#include "Phi4_library_SINGLE_realspace.cuh"

	static int ConfigureRandomNumbers(void) {
		/* Allocate memory for RNG's*/
		unsigned long long h_x[NUM_THREADS];
		unsigned int h_a[NUM_THREADS];
		unsigned long long seed = (unsigned long long) SEED;

		/* Init RNG's*/
		int error = init_RNG(h_x, h_a, NUM_THREADS, SAFE_PRIMES_FILENAME, seed);

		if (!error) {
			size_t size_x = NUM_THREADS * sizeof(unsigned long long);
			CUDA_SAFE_CALL(cudaMemcpyToSymbol(d_x, h_x, size_x));

			size_t size_a = NUM_THREADS * sizeof(unsigned int);
			assert(size_a<size_x);
			CUDA_SAFE_CALL(cudaMemcpyToSymbol(d_a, h_a, size_a));
		}

		return error;
	}

#else

	// Including our functions
	#include "Phi4_library_SINGLE_realspace-PHILOX.cuh"

#endif

/////////////////////////////////////////////////////////
int main(){
	assert(TILE_X%2==0);
	assert(TILE_Y%2==0);
	assert(FRAME%TILE_X==0);
	assert(FRAME%TILE_Y==0);
	assert(LX%FRAME==0);
	assert(LY%FRAME==0);
	assert(T_RUN%T_DATA==0);
	assert(DT<=0.5);	//We've assumed a small temporal step in the integration scheme

	// Set the GPGPU computing device
	CUDA_SAFE_CALL(cudaSetDevice(CUDA_DEVICE));

	// Choosing less "shared memory", more cache.
	CUDA_SAFE_CALL(cudaThreadSetCacheConfig(cudaFuncCachePreferL1)); 
	
	double secs = 0.0;
	struct timeval start = {0L,0L}, end = {0L,0L}, elapsed = {0L,0L};

	// Start timer
	gettimeofday(&start, NULL);

	// Print header
	printf("# Lx: %i\n", LX);
	printf("# Ly: %i\n", LY);
	printf("# Running time (in Euler steps): %i\n", T_RUN);
	printf("# Data Acquiring Step: %i\n", T_DATA);
	printf("# alpha: %f\n", C_ALPHA);
	printf("# beta: %f\n", C_BETA);
	printf("# epsilon: %f\n", C_EPSILON);
	printf("# DT for Euler step: %f\n", DT);
	printf("# Temperature: %f\n", TEMP);
	printf("# External field: %f\n", H_START);
	printf("# Parabola Velocity: %f\n", VELOCITY);
	printf("# SEED used: %i\n", SEED);

#ifndef PHILOX
	if (ConfigureRandomNumbers()) {
		return 1;
	}
#endif

	// main{} variables
	char filename [200];
	FLOAT h_field = 2.*M_PI/LY*H_START;
	FLOAT offset = 0.95;
	FLOAT velocity = VELOCITY; //0.05;

	// Our class
	phi4_model T;

	/* Choose a particular initial condition for the line (to be done in host and then copied to device)*/
	// USE: InitParticular(Lx, Ly, shape, x_offset in (-.5,.5), y_offset in (-.5,.5), width in (0,1) prop to LY, angle in (0,PI/2)) 
	// shape: 0=flat_interfase; 1=band, 2=circle, 3=square_with_hole, ..... more to come
	//T.InitParticular(LX,LY,4,0.,0.,2*C_BETA/C_GAMMA/LY,M_PI/6.);
	//T.InitParticular(LX,LY,1,0.,0.,2*C_BETA/C_GAMMA/LY,M_PI/6.);
	T.InitParticular(LX,LY,0,0.,offset-0.5,0.1,0.);

	/* Homegeneous initial condition for the scalar field (to be done in device)*/
	// USE: InitFlat(value)
	//T.InitFlat(0.);

	/* Random initial condition for the scalar field (to be done in device)*/
	//T.InitRandom();

	/* Calculate discretized Fourier modes */
	//T.SetQxQy();

#ifndef PHILOX
	/* Claculate the quenched disorder */
	T.SetDisorder();
#endif

	//TODO: Add a transient loop to preapare the system in a equilibrium state if desired.

	float disordershiftx = 0;
	int nwalls;
	for(int i=0;i<T_RUN+1; i++){

		// if accumulate
		//nwalls += T.CountInterfaces(i) - 2*LX;

		/* Get data each T_DATA steps*/
		if(i%T_DATA==0) {
			T.CpyDeviceToHost(); // To be used in a moment for printing. Taking advantage of asynchronicity.
			nwalls = T.CountInterfaces(i);
			std::cout << i << " " << nwalls - 2*LX << std::endl;
			// if accumulate
			//if (i!=0) std::cout << i << " " << nwalls/(float)T_DATA << std::endl;
			//nwalls = 0;

			/* Calculate forces in real space*/
			T.CalculateRealForce(h_field, offset, disordershiftx);

			/* Integration step in Real Space*/
			T.EulerStep();
			//T.EulerStepSemiimplicit();
			disordershiftx+=velocity*DT;

			#ifdef MOVIE
			/* Print frame for visualization */
			sprintf(filename, "frame%d.ppm", 100000000+i);
			ifstream file(filename);
			T.PrintPicture(filename,i);
			#endif
			
		}
		else{
			/* Calculate forces in real space*/
			T.CalculateRealForce(h_field, offset, disordershiftx);

			/* Integration step in Real Space */
			T.EulerStep();
			//T.EulerStepSemiimplicit();
			disordershiftx+=velocity*DT;
		}
	}

	// Stop timer
	gettimeofday(&end,NULL);
	timeval_subtract(&elapsed, &end, &start);
	secs = (double)elapsed.tv_sec + ((double)elapsed.tv_usec*MICROSEC);
	printf("# System size: LX=%i LY=%i . Phi4CUDA execution time (in secs): %lf \n", LX, LY, secs);

	return 0;
}

/*
 * http://www.gnu.org/software/libtool/manual/libc/Elapsed-Time.html
 * Subtract the `struct timeval' values X and Y,
 * storing the result in RESULT.
 * Return 1 if the difference is negative, otherwise 0.
 */

int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y) {
	/* Perform the carry for the later subtraction by updating y. */
	if (x->tv_usec < y->tv_usec) {
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_usec - y->tv_usec > 1000000) {
		int nsec = (x->tv_usec - y->tv_usec) / 1000000;
		y->tv_usec += 1000000 * nsec;
		y->tv_sec -= nsec;
	}

	/* Compute the time remaining to wait. tv_usec is certainly positive. */
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_usec = x->tv_usec - y->tv_usec;

	/* Return 1 if result is negative. */
	return x->tv_sec < y->tv_sec;
}

