/*
 * cufftw_wrap-phi4.hpp
 *
 *  Created on: Jul 11, 2011
 *      Author: eze, ale
 */

#ifndef FFTW_WRAP_HPP_
#define FFTW_WRAP_HPP_

//#include "../common/common.c"
#include <stdint.h> /* uint8_t */

#include <thrust/transform_reduce.h>
#include <thrust/extrema.h>
#include <thrust/device_vector.h>
#include <thrust/unique.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/generate.h>
#include <thrust/random.h>
#include <thrust/transform.h>
#include <thrust/remove.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/count.h>
#include <thrust/functional.h>
#include <thrust/sequence.h>
#include <thrust/fill.h>
#include <thrust/partition.h>

using namespace thrust;
using namespace std;

// THRUST PREDICATES

struct is_positive: public thrust::unary_function<int,bool>
{
    __host__ __device__
    bool operator()(const int v)
    {
      return (v > 0.);
    }
};

struct is_wall: public thrust::unary_function<tuple<float,float,int>,int>
{
    __host__ __device__
    int operator()(const tuple<float,float,int> v)
    {
      float v1 = get<0>(v);
      float v2 = get<1>(v);
      int i = get<2>(v);
//      int res = (v1 * v2 <= 0.f && v1!=0.f)?(i):(-1);
	int res = ( ((v1>0) && (v2<=0)) || ((v1<0) && (v2>=0)) )?(i):(-1);
      return res;
    }
};

struct is_left_wall
  {
    __host__ __device__
    bool operator()(const int &x)
    {
      return (x < Ly/2);
    }
};

// THRUST FUNCTIONS

// square<T> computes the square of a number f(x) -> x*x
template <typename T1>
struct square : public thrust::unary_function<T1,T1>
{
    __host__ __device__
        T1 operator()(const T1& x) const {
            return x * x;
        }
};


// typedef iterators para evitar declaracion choclo
typedef device_vector<int>::iterator   IntIterator;
typedef device_vector<float>::iterator FloatIterator;
typedef counting_iterator<int>   CountIterator;
typedef tuple<FloatIterator,FloatIterator,CountIterator> ItTuple;
typedef zip_iterator<ItTuple> ZipIt;
typedef transform_iterator<is_wall,ZipIt> transf_iter_is_wall;

texture<float,1,cudaReadModeElementType> Qtexture;
texture<float,1,cudaReadModeElementType> Etexture;


__global__ void kernel_init_random(cufftReal *a, int nx, int ny){
	const unsigned int jAux = blockIdx.x*TILE_X + threadIdx.x;
	const unsigned int iAux = blockIdx.y*TILE_Y + threadIdx.y;
	const unsigned int tid = iAux*FRAME + jAux;
	// move thread RNG state to registers. 
	unsigned long long rng_state = d_x[tid];
	const unsigned int rng_const = d_a[tid];

	unsigned int i;	
	for (unsigned int iFrame=0; iFrame<(nx/FRAME); iFrame++) {
		i = iAux + FRAME*iFrame;
		unsigned int j;
		for (unsigned int jFrame=0; jFrame<(ny/FRAME); jFrame++) {
			j = jAux + FRAME*jFrame;	
			// compute index with i and j, the location of the element in the original Lx*Ly array 
			int index = i*ny + j;
			a[index] = 2.f*(rand_MWC_co(&rng_state, &rng_const)-0.5);
		}
	}	
	d_x[tid] = rng_state; // store RNG state into global again		
}


__global__ void kernel_set_epsilon(float *d_epsilon, int nx, int ny){
	const unsigned int jAux = blockIdx.x*TILE_X + threadIdx.x;
	const unsigned int iAux = blockIdx.y*TILE_Y + threadIdx.y;
	const unsigned int tid = iAux*FRAME + jAux;
	// move thread RNG state to registers. 
	unsigned long long rng_state = d_x[tid];
	const unsigned int rng_const = d_a[tid];

	unsigned int i;	
	for (unsigned int iFrame=0; iFrame<(nx/FRAME); iFrame++) {
		i = iAux + FRAME*iFrame;
		unsigned int j;
		for (unsigned int jFrame=0; jFrame<(ny/FRAME); jFrame++) {
			j = jAux + FRAME*jFrame;	
			// compute index with i and j, the location of the element in the original Lx*Ly array 
			int index = i*ny + j;
			d_epsilon[index] = Calpha + Cepsilon*2.f*(rand_MWC_co(&rng_state, &rng_const)-0.5);
		}
	}	
	d_x[tid] = rng_state; // store RNG state into global again		
}


__global__ void kernel_set_qx_qy(float *qqmodule, int nx, int ny){
	// compute idx and idy, the location of the element in the original Lx*Ly array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		//int nxs2=(nx/2); int nys2=(ny/2);
		// cuando usamos los cosenos no es necesario meter ningún módulo, las cc quedan determinadas por la periodicidad de la funcion
		float qx=2.f*M_PI/(float)Lx * idx;
		float qy=2.f*M_PI/(float)Ly * idy;
		//float qx=2*M_PI/nx*((idx+nxs2)-nx);
		//float qy=2*M_PI/ny*((idy+nys2)-ny);
		//int index = ((idx+nxs2)%nx)*ny + ((idy+nys2)%ny); 
		int index = idx*ny + idy;
		//qqmodule[index]=qx*qx+qy*qy;
		qqmodule[index]=-2.f*(cosf(qx)+cosf(qy)-2.f);
	}
}

__global__ void Real2Real_scaled(cufftReal *a, int nx, int ny, float scale){
	// compute idx and idy, the location of the element in the original Lx*Ly array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){	
		int index = idx*ny + idy;
		a[index]= scale*a[index];
	}	
}

__global__ void complex2complex_scaled(cufftComplex *a, int nx, int ny, float scale){
	// compute idx and idy, the location of the element in the original Lx*Ly array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){	
		int index = idx*ny + idy;
		a[index].x= scale*a[index].x;
		a[index].y= scale*a[index].y;
	}	
}


__global__ void recalculatingRealForce(const float *e, const cufftReal *a, cufftReal *b, int nx, int ny, float uniform_field){
	const unsigned int jAux = blockIdx.x*TILE_X + threadIdx.x;
	const unsigned int iAux = blockIdx.y*TILE_Y + threadIdx.y;
	const unsigned int tid = iAux*FRAME + jAux;
	// move thread RNG state to registers. 
	unsigned long long rng_state = d_x[tid];
	const unsigned int rng_const = d_a[tid];

	unsigned int i;	
	for (unsigned int iFrame=0; iFrame<(nx/FRAME); iFrame++) {
		i = iAux + FRAME*iFrame;
		unsigned int j;
		for (unsigned int jFrame=0; jFrame<(ny/FRAME); jFrame++) {
			j = jAux + FRAME*jFrame;
			// compute index with i and j, the location of the element in the original Lx*Ly array 
			int index = i*ny + j;
 			//const float epsilon = e[index];
			const float epsilon = tex1Dfetch(Etexture, index);
			b[index] = epsilon*(a[index]-a[index]*a[index]*a[index]) + xtemp*(rand_MWC_co(&rng_state, &rng_const)-0.5) + uniform_field;
		}	
	}
	d_x[tid] = rng_state; // store RNG state into global again		
}

__global__ void euler_step(cufftComplex *a, const cufftComplex *b, const float *qqmodule, int nx, int ny){
	// compute idx and idy, the location of the element in the original NxN array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx && idy < ny){
		int index = idx*ny + idy;
		//const float fac=-(qqmodule[index]);
		const float fac=-(tex1Dfetch(Qtexture, index));
		const float dip = -(9.05f-2.f*M_PI*sqrtf(-1.f*fac));
		a[index].x = (1.f+Cbeta*fac*Dt+Cgamma*dip*Dt)*a[index].x + Dt*b[index].x;
		a[index].y = (1.f+Cbeta*fac*Dt+Cgamma*dip*Dt)*a[index].y + Dt*b[index].y;
	}	
}

__global__ void euler_semiimplicit_step(cufftComplex *a, const cufftComplex *b, const float *qqmodule, int nx, int ny){
	// compute idx and idy, the location of the element in the original NxN array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx  && idy < ny){
		int index = idx*ny + idy;
		//const float fac=-(qqmodule[index]);
		const float fac=-(tex1Dfetch(Qtexture, index));
		const float dip = -(9.05f-2.f*M_PI*sqrtf(-1.f*fac));
		a[index].x = (a[index].x*(1.f+Cgamma*dip*Dt) + Dt*b[index].x)/(1.f-fac*Cbeta*Dt);
		a[index].y = (a[index].y*(1.f+Cgamma*dip*Dt) + Dt*b[index].y)/(1.f-fac*Cbeta*Dt);
	}
}


__global__ void structure_factor_CUDA(const cufftComplex *a, float *Sdistr, unsigned int* Sdistr_count, int nx, int ny){
	// compute idx and idy, the location of the element in the original NxN array 
	const unsigned int idx = blockIdx.x*blockDim.x+threadIdx.x;
	const unsigned int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx  && idy < ny){
		const unsigned int index = idx*ny + idy;

		const unsigned int kx = idx/2;
		const unsigned int ky = idy+(idx%2)*ny;	

		const unsigned int kmoduleindex = kx*kx+ky*ky;
		//const unsigned int kmoduleindex = idx;
		//const unsigned int kmoduleindex = idx*idx + idy*idy;
		//const float kmodule= (tex1Dfetch(Qtexture, index));
		const float phiaux = a[index].x*a[index].x + a[index].y*a[index].y ;
		atomicAdd(&Sdistr[kmoduleindex],phiaux);
		atomicAdd(&Sdistr_count[kmoduleindex],1);

		//kmoduleindex = idx*idx + (Ly-idy)*(Ly-idy);
		//atomicAdd(&Sdistr[kmoduleindex],phiaux);
		//atomicAdd(&Sdistr_count[kmoduleindex],1);

	}
}


/* RGB formulae.
 * http://gnuplot-tricks.blogspot.com/2009/06/comment-on-phonged-surfaces-in-gnuplot.html
 * TODO: cannot find what to do with over/under flows, currently saturate in [0,255]
 *
 * there are 37 available rgb color mapping formulae:
 * 0: 0               1: 0.5             2: 1
 * 3: x               4: x^2             5: x^3
 * 6: x^4             7: sqrt(x)         8: sqrt(sqrt(x))
 * 9: sin(90x)        10: cos(90x)       11: |x-0.5|
 * 12: (2x-1)^2       13: sin(180x)      14: |cos(180x)|
 * 15: sin(360x)      16: cos(360x)      17: |sin(360x)|
 * 18: |cos(360x)|    19: |sin(720x)|    20: |cos(720x)|
 * 21: 3x             22: 3x-1           23: 3x-2
 * 24: |3x-1|         25: |3x-2|         26: (3x-1)/2
 * 27: (3x-2)/2       28: |(3x-1)/2|     29: |(3x-2)/2|
 * 30: x/0.32-0.78125 31: 2*x-0.84       32: 4x;1;-2x+1.84;x/0.08-11.5
 * 33: |2*x - 0.5|    34: 2*x            35: 2*x - 0.5
 * 36: 2*x - 1
 * 
 * Some nice schemes in RGB color space
 * 7,5,15   ... traditional pm3d (black-blue-red-yellow)
 * 3,11,6   ... green-red-violet
 * 23,28,3  ... ocean (green-blue-white); try also all other permutations
 * 21,22,23 ... hot (black-red-yellow-white)
 * 30,31,32 ... color printable on gray (black-blue-violet-yellow-white)
 * 33,13,10 ... rainbow (blue-green-yellow-red)
 * 34,35,36 ... AFM hot (black-red-yellow-white)
 */

#define RGBf05 (x*x*x)
#define RGBf07 (sqrt(x))
#define RGBf15 (sin(2*M_PI*x))
#define RGBf21 (3*x)
#define RGBf22 (3*x-1)
#define RGBf23 (3*x-2)
#define RGBf30 (x/0.32-0.78125)
#define RGBf31 (2*x-0.84)
#define RGBf32 (x/0.08-11.5)

#define MAX_COMPONENT_VALUE 255
//void writePPMbinaryImage(const char * filename, const cufftComplex * vector)
void writePPMbinaryImage(const char * filename, const cufftReal * vector)
{
	FILE *f = NULL;
	unsigned int i = 0, j = 0;
	uint8_t RGB[3] = {0,0,0}; /* use 3 first bytes for BGR */ 
	f = fopen(filename, "w");
	assert(f);
	fprintf(f, "P6\n"); /* Portable colormap, binary */
	fprintf(f, "%d %d\n", Ly, Lx); /* Image size */
	fprintf(f, "%d\n", MAX_COMPONENT_VALUE); /* Max component value */
	for (i=0; i<Lx; i++){
		for (j=0; j<Ly; j++){
			/* ColorMaps
			 * http://mainline.brynmawr.edu/Courses/cs120/spring2008/Labs/ColorMaps/colorMap.py
			 */
			//float x = vector[i*Ly+j].x; //HOT; /* [0,1] */
			float x = vector[i*Ly+j]; //HOT; /* [0,1] */			
			RGB[0] = (uint8_t) MIN(255, MAX(0, 256*RGBf21));
			RGB[1] = (uint8_t) MIN(255, MAX(0, 256*RGBf22));
			RGB[2] = (uint8_t) MIN(255, MAX(0, 256*RGBf23));
			//assert(0<=RGB[0] && RGB[0]<256);
			//assert(0<=RGB[1] && RGB[1]<256);
			//assert(0<=RGB[2] && RGB[2]<256);
			fwrite((void *)RGB, 3, 1, f);
		}
	}
	fclose(f); f = NULL;
	return;
}


class cufft_complex_to_complex_2d
{
	private:
	// plan
	cufftHandle Sp, Sr2c, Sc2r; 

	public:
	// arrays complejos y reales para poner el campo escalar y la fuerza
	cufftComplex *d_phi, *d_U; 
	cufftReal *h_phi_r, *h_U_r;
	cufftReal *d_phi_r, *d_U_r;
	
	//los qs
	float *d_qmodule; 
	
	//el desorden
	float *d_epsilon;
	
	//la posicion, el ancho
	double *position_M, *width_M;
	unsigned int *time_M;
	
    // inicializacion de la transformada
    cufft_complex_to_complex_2d(){

		h_phi_r = (cufftReal *)malloc(sizeof(cufftReal)*Lx*Ly);
		h_U_r = (cufftReal *)malloc(sizeof(cufftReal)*Lx*Ly);
	
		position_M = (double *)malloc(sizeof(double)*(NORD*MAXORD));  //(Trun/Tframe)); 
		width_M = (double *)malloc(sizeof(double)*(NORD*MAXORD));  //(Trun/Tframe));
		time_M = (unsigned int *)malloc(sizeof(unsigned int)*(NORD*MAXORD));  //(Trun/Tframe));

		CUDA_SAFE_CALL(cudaMalloc((void**)&d_phi, sizeof(cufftComplex)*Lx*(Ly/2+1)));
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_U, sizeof(cufftComplex)*Lx*(Ly/2+1)));

		CUDA_SAFE_CALL(cudaMalloc((void**)&d_phi_r, sizeof(cufftReal)*Lx*Ly));
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_U_r, sizeof(cufftReal)*Lx*Ly));
		
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_qmodule, sizeof(float)*Lx*(Ly/2+1))); 

		CUDA_SAFE_CALL(cudaMalloc((void**)&d_epsilon, sizeof(float)*Lx*Ly)); 
		
		// plans R2C and C2R
		CUFFT_SAFE_CALL(cufftPlan2d(&Sr2c,Lx,Ly,CUFFT_R2C));
		CUFFT_SAFE_CALL(cufftPlan2d(&Sc2r,Lx,Ly,CUFFT_C2R));
		
	//	cufftSetCompatibilityMode(Sr2c, CUFFT_COMPATIBILITY_NATIVE);
	//	cufftSetCompatibilityMode(Sc2r, CUFFT_COMPATIBILITY_NATIVE);
    }


	void set_qx_qy(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(Lx/TILE_X, Ly/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		kernel_set_qx_qy<<<dimGrid, dimBlock>>>(d_qmodule,Lx,Ly/2+1);
		CUDA_SAFE_CALL(cudaBindTexture(NULL, Qtexture, d_qmodule, Lx*(Ly/2+1)*sizeof(float)));
		
	};

	void set_epsilon(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(FRAME/TILE_X, FRAME/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		kernel_set_epsilon<<<dimGrid, dimBlock>>>(d_epsilon,Lx,Ly);
		CUDA_SAFE_CALL(cudaBindTexture(NULL, Etexture, d_epsilon, Lx*Ly*sizeof(float)));		
	};

	void init_width()
	{
		//for(int index=0;index<Trun/Tframe;index++)
		for(int index=0;index<NORD*MAXORD;index++)
		{
		position_M[index]=0.0;
		width_M[index]=0.0;
		time_M[index]=0;		
		}
	};
	
	void init_random(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(FRAME/TILE_X, FRAME/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		kernel_init_random<<<dimGrid, dimBlock>>>(d_phi_r,Lx,Ly);
	};

	void init_flat(){
	       CUDA_SAFE_CALL(cudaMemset(d_phi, 0, Lx*(Ly/2+1)*sizeof(cufftComplex)));
	       CUDA_SAFE_CALL(cudaMemset(d_U, 0, Lx*(Ly/2+1)*sizeof(cufftComplex)));
	       
      	       CUDA_SAFE_CALL(cudaMemset(d_phi_r, 0, Lx*Ly*sizeof(cufftReal)));
	       CUDA_SAFE_CALL(cudaMemset(d_U_r, 0, Lx*Ly*sizeof(cufftReal))); 
	};	


	void init_particular(int nx, int ny){
		for(int i=0;i<nx;i++){
			for(int j=0;j<ny;j++){
				int k=i*ny+j;

				//circulo al diome
 				//h_phi_r[k]= ((nx/2-i)*(nx/2-i)+(ny/2-j)*(ny/2-j) < 10000.)?(1.0):(-1.0);				

				//linea
// 				h_phi[k]_r= (0 <= j && j < ny*0.05)?(1.0):(-1.0);
				
				//banda
				h_phi_r[k]= (ny*0.25 < j && j < ny*0.75)?(1.0):(-1.0);
			}
		}
	}

	/* transfer from CPU to GPU memory */
	void Cpy_Host_to_Device(){
		CUDA_SAFE_CALL(cudaMemcpy(d_phi_r,h_phi_r, sizeof(cufftReal)*Lx*Ly, cudaMemcpyHostToDevice));
		CUDA_SAFE_CALL(cudaMemcpy(d_U_r,h_U_r, sizeof(cufftReal)*Lx*Ly, cudaMemcpyHostToDevice));			
	}	

	/* transfer from GPU to CPU memory */
	void Cpy_Device_to_Host(){	
		CUDA_SAFE_CALL(cudaMemcpy(h_phi_r, d_phi_r, sizeof(cufftReal)*Lx*Ly, cudaMemcpyDeviceToHost));	
	}

	void transformar()
	{
		CUFFT_SAFE_CALL(cufftExecR2C(Sr2c, d_phi_r, d_phi));
		CUFFT_SAFE_CALL(cufftExecR2C(Sr2c, d_U_r, d_U));
	};

	void antitransformar()
	{
		CUFFT_SAFE_CALL(cufftExecC2R(Sc2r, d_phi, d_phi_r)); 	
	};	
	
	/* apply scaling ( an FFT followed by iFFT will give you back the same array times the length of the transform) */
	void normalize(void){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(Lx/TILE_X, Ly/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		Real2Real_scaled<<<dimGrid, dimBlock>>>(d_phi_r, Lx, Ly, 1.f / ((float) Lx * (float) Ly));
	}

	void recalcularRealForce(float uniform_field){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(FRAME/TILE_X, FRAME/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);		

		recalculatingRealForce<<<dimGrid, dimBlock>>>(d_epsilon,d_phi_r,d_U_r,Lx,Ly,uniform_field);	
	}	


	void print_input(ofstream &fstr)
	{
		//fstr.seekp(ios_base::beg);

		for(int i=0;i<Lx;i++)
		{
			for(int j=0;j<Ly;j++)
			{
				int k=i*Ly+j;
				fstr << setprecision (4) << h_phi_r[k] << " " ;
			}
			fstr << endl;
		}
		fstr << endl << endl;

	};
	
	
	void print_result(ofstream &fstr)
	{
		for(int index=0;index<Trun/Tframe;index++)
		{
 		fstr << index*Tframe << " " << position_M[index]/(float) SAMPLES << " " << width_M[index]/(float) SAMPLES <<endl;	
		}
		fstr << endl;
	};
	
	void print_result2(ofstream &fstr)
	{
		for(int index=1;index<NORD*MAXORD;index++){
	 		if (position_M[index]!=0) fstr << time_M[index] << " " << position_M[index]/(float) SAMPLES << " " << width_M[index]/(float) SAMPLES <<endl;	
		}
		fstr << endl;
	};


	void print_dibujito(char * picturename, int x){
		char cmd[240];	char cmd2[240]; char picturenameppm[240];
		sprintf(picturenameppm, "%s.ppm", picturename);
		writePPMbinaryImage(picturenameppm, h_phi_r);	
	//	sprintf(cmd, "convert frame%d.ppm frame%d.jpg", 100000000+x, 100000000+x);
		sprintf(cmd, "convert %s.ppm %s.jpg", picturename, picturename);
		std::system(cmd);
	//	sprintf(cmd2, "rm frame%d.ppm", 100000000+x);
		sprintf(cmd2, "rm %s.ppm", picturename);
		std::system(cmd2);	
	};
	

	void medir_factor_estructura(){
		
		int numbins = Lx*Lx + (Ly/2+1)*(Ly/2+1);
	
		float *d_Sdistr = NULL;
		unsigned int *d_Sdistr_count = NULL;

		size_t sizef = numbins * sizeof(float);
		CUDA_SAFE_CALL(cudaMalloc((void**) &d_Sdistr, sizef));
	        CUDA_SAFE_CALL(cudaMemset(d_Sdistr, 0, sizef));

		size_t sizei = numbins * sizeof(unsigned int);
		CUDA_SAFE_CALL(cudaMalloc((void**) &d_Sdistr_count, sizei));
	        CUDA_SAFE_CALL(cudaMemset(d_Sdistr_count, 0, sizei));

		float *h_Sdistr = NULL;
		unsigned int *h_Sdistr_count = NULL;
		h_Sdistr = (float *)malloc(sizef);
		h_Sdistr_count = (unsigned int *)malloc(sizei);

		/*
		for (unsigned int i=0; i<numbins; i++){
		h_Sdistr[i] = 0.f;
		h_Sdistr_count[i] = 0;
		}
		*/

		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(Lx/TILE_X, Ly/2/TILE_Y+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		structure_factor_CUDA<<<dimGrid, dimBlock>>>(d_phi, d_Sdistr, d_Sdistr_count, Lx, (Ly/2)+1);
		CUT_CHECK_ERROR("Kernel structure_factor execution failed");

		CUDA_SAFE_CALL(cudaMemcpy(h_Sdistr, d_Sdistr, numbins*sizeof(float), cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_Sdistr_count, d_Sdistr_count, numbins*sizeof(unsigned int), cudaMemcpyDeviceToHost));

		for (unsigned int i=1; i<2*Lx; i++) { //en realidad el array abarca hasta i<numbins
		//if (h_Sdistr[i] !=0 && h_Sdistr_count[i] !=0 && (i%2==1)) cout << i << " " << h_Sdistr[i]/(float)h_Sdistr_count[i]/(float)(Lx*Ly) << endl;
		if (h_Sdistr[i] !=0 && (i%2==1)) cout << i << " " << h_Sdistr[i]/(float)(Lx*Ly) << endl;
		//cout << i << " " << h_Sdistr_count[i] << endl;
		//if (h_Sdistr[i] > 0.000001) cout << i << " " << h_Sdistr[i] << endl;
		}
		cout << endl;

		for (unsigned int i=1; i<2*Lx; i++) { //en realidad el array abarca hasta i<numbins
		//if (h_Sdistr[i] !=0 && h_Sdistr_count[i] !=0 && (i%2==0)) cout << i << " " << h_Sdistr[i]/(float)h_Sdistr_count[i]/(float)(Lx*Ly) << endl;
		if (h_Sdistr[i] !=0 && (i%2==0)) cout << i << " " << h_Sdistr[i]/(float)(Lx*Ly) << endl;
		}
		cout << endl;


	}

	int detecta_paredes_paralelo(int index, unsigned int time){
		
		time_M[index]=time;

		int ret=0;			
	
		const unsigned int N=Lx*Ly;
		const unsigned int M = 2*Lx; //la cantidad de paredes que esperamos tener

       		thrust::device_ptr<cufftReal> dev_ptr(d_phi_r);	

		unsigned int *d_walls_location = NULL;
		size_t size = 2*Lx * sizeof(unsigned int);
		CUDA_SAFE_CALL(cudaMalloc((void**) &d_walls_location, size));
       		thrust::device_ptr<unsigned int> w_loc(d_walls_location);

		FloatIterator itD_begin, itD_shift_begin;
	    	FloatIterator itD_end, itD_shift_end;
		
		counting_iterator<int> index_begin(0);
		counting_iterator<int> index_end(N-1);

		itD_begin = dev_ptr; itD_end = dev_ptr+N-1;
	    	itD_shift_begin = dev_ptr+1; itD_shift_end = dev_ptr+N;

		device_vector<int> res(2*M); /*el tamaño de res podría ser mayor que M, en ese caso va a saltar el assert de más abajo*/

	    	transf_iter_is_wall it0b(
			make_zip_iterator(make_tuple(itD_begin, itD_shift_begin, index_begin)), 
			is_wall()
		);
	    	transf_iter_is_wall it1b(
			make_zip_iterator(make_tuple(itD_end, itD_shift_end, index_end)),
			is_wall()
	    	);

		//int nparedes =  N-1-thrust::count(it0b, it1b,-1);
		//assert(nparedes==2*Lx);		
		//std::cout << "hay " << nparedes << " paredes" << std::endl;

		IntIterator res_fin = thrust::copy_if(it0b, it1b, res.begin(),is_positive());
		int nparedes =  res_fin - res.begin();
		//assert(nparedes==2*Lx);
		if (nparedes!=2*Lx){
		printf("# OVERHANG! time= %i , paredes encontradas %i \n", index*Tframe, nparedes);
		ret=1;
		}		
	   	//std::cout << "hay " << nparedes << " paredes" << std::endl;

		/* meto el vector con las localizaciones de paredes en w_loc */
	    	thrust::copy(res.begin(), res_fin, w_loc);

	/* lo que sigue hace la reduccion de las paredes en la GPU*/

/*		// tomo %Ly para obtener las posiciones en cada fila 
		thrust::device_vector<unsigned int> V2(M);
		thrust::fill(V2.begin(), V2.end(), Ly);
		thrust::transform(w_loc, w_loc+M, V2.begin(), w_loc, thrust::modulus<unsigned int>());

		// reordeno para tener primero todas las posiciones de la pared izquierda y a continuacion las de la derecha
		thrust::partition(w_loc, w_loc+M, is_left_wall());

		// sumo/resto por Ly/2 según corresponda para tener posiciones respecto al centro 
		thrust::fill(V2.begin(), V2.end(), Ly/2);
		thrust::transform(thrust::make_transform_iterator(w_loc, thrust::negate<unsigned int>()), 
				thrust::make_transform_iterator(w_loc+Ly, thrust::negate<unsigned int>()), 
				V2.begin(), w_loc, thrust::plus<unsigned int>());
		thrust::transform(w_loc+Ly, w_loc+M, V2.begin(), w_loc+Ly, thrust::minus<unsigned int>());

		const unsigned int pos = thrust::reduce(w_loc, w_loc+M);
		const double position = pos/(float)(M);

		thrust::device_vector<double> V3(M);
		thrust::fill(V3.begin(), V3.end(), position);
		thrust::transform(w_loc, w_loc+M, V3.begin(), w_loc, thrust::minus<double>());
		const double widht = thrust::transform_reduce(w_loc, w_loc+M, square<double>(), 0, thrust::plus<double>());
		
		//unsigned long long pos2 = thrust::transform_reduce(w_loc, w_loc+M, square<unsigned int>(), 0, thrust::plus<unsigned int>());
		//double position2 = pos2/(float)(M*M);
		//double widht = position2 - position*position;
				
		//debugging
		//unsigned int sum1 = thrust::reduce(w_loc, w_loc+Lx);
		//unsigned int sum2 = thrust::reduce(w_loc+Lx, w_loc+M);
		//cout << index << " " << sum1/(float)Lx << " " << sum2/(float)Lx << endl;
		//cout << index << " " << pos/(float)(2*Lx) << endl;

		position_M[index]+= position;
		width_M[index]+= widht/(float)(M);
*/

	/* lo que sigue hace la reduccion de las paredes en la CPU*/

		unsigned int *h_walls_location = NULL;
		h_walls_location=(unsigned int*)malloc(size);

		CUDA_SAFE_CALL(cudaMemcpy(h_walls_location, d_walls_location, size, cudaMemcpyDeviceToHost));
		
		unsigned int tmp_position1 = 0;
		unsigned int tmp_position2 = 0;
		for (unsigned int i=0; i<Lx; i++) {
			//tmp_position1 += Ly/2 - h_walls_location[2*i];
			//tmp_position2 += h_walls_location[2*i+1] - Ly/2;
			tmp_position1 += Ly/2 - h_walls_location[2*i]%Ly;
			tmp_position2 += h_walls_location[2*i+1]%Ly - Ly/2;
		//cout << index*Tframe << " " << h_walls_location[2*i] << " " << h_walls_location[2*i+1] << endl;
		}

		//h_position1[index] = tmp_position1/(float)Lx;
		//h_position2[index] = tmp_position2/(float)Lx;	
		//position_M[index]+= (h_position1[index] + h_position2[index])/2.;
		//cout << index*Tframe << " " << tmp_position1/(float)(Lx) << " " << tmp_position2/(float)(Lx) << endl;
		position_M[index]+= (tmp_position1/(float)Lx + tmp_position2/(float)Lx)/2.;

		unsigned int tmp_width1 = 0;
		unsigned int tmp_width2 = 0;
		int aux; 
		for (unsigned int i=0; i<Lx; i++) {
			aux=(Ly/2 - h_walls_location[2*i]%Ly - tmp_position1/(float)Lx);
			tmp_width1 += aux*aux;
			aux=(h_walls_location[2*i+1]%Ly - Ly/2 - tmp_position2/(float)Lx);
			tmp_width2 += aux*aux;
		}

		//cout << index*Tframe << " " << tmp_width1/(float)(Lx) << " " << tmp_width2/(float)(Lx) << endl;
		width_M[index]+= (tmp_width1/(float)Lx + tmp_width2/(float)Lx)/2.;
	

	return(ret);

	}


	void euler(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(Lx/TILE_X, Ly/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);		
		euler_step<<<dimGrid, dimBlock>>>(d_phi, d_U, d_qmodule, Lx, (Ly/2+1));
	};	
	
	void euler_semiimplicit(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(Lx/TILE_X, Ly/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		euler_semiimplicit_step<<<dimGrid, dimBlock>>>(d_phi, d_U, d_qmodule, Lx, (Ly/2+1));
	};
	
	~cufft_complex_to_complex_2d(){
		cufftDestroy(Sp);
		cudaFree(d_phi);
		cudaFree(d_U);
		cudaFree(d_phi_r);
		cudaFree(h_phi_r);
		cudaFree(d_U_r);
		cudaFree(h_U_r);
	
	};

};


#endif /* FFTW_WRAP_HPP_ */
